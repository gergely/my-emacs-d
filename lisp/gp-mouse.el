;;; gp-mouse --- Extra mouse functionality

;;; Commentary:

;;; Code:
(require 'mouse)

(defun gpolonkai/event-in-current-window-p (event)
  "Check if EVENT happened in the current window."
  (let ((current-window (selected-window))
        (event-window (posn-window (event-start event))))
    (eq current-window event-window)))

(defun gpolonkai/mouse-set-point (click &optional promote-to-region)
  "Set mouse position.

If CLICK happened in an inactive window, select that window without setting point"
  (interactive "e\np")
  (if (gpolonkai/event-in-current-window-p click)
      (call-interactively 'mouse-set-point)
    (call-interactively 'mouse-select-window)))

(global-set-key [mouse-1] 'mouse-select-window-or-set-point)
(global-unset-key [down-mouse-1])

(provide 'gp-mouse)
;;; gp-mouse.el ends here
