;;; init --- Summary

;;; Commentary:

;;; Code:

(setq custom-file (concat user-emacs-directory "customizations.el"))
(load custom-file)

;; Initialize the package system and use-package
(setq load-prefer-newer t)

;; I started moving my configuration to this Org file.  It’s easier to document this way.
(org-babel-load-file (expand-file-name "configuration.org" user-emacs-directory))

;;; init.el ends here
